#!/usr/bin/bash
echo
echo This program is designed to change the oauth2 token
echo Make sure you are running this script from within the repository you want to affect
echo "Otherwise just do \"cd <my-proj-directory>\""
echo

echo First we need need the git url - it can be found under \
CLONE WITH HTTPS on your git repo\'s website: in the form https://gitlab.com/company/project/folder/file.git

echo NOTE: detault is current repo\'s url. So press enter if you don\'t want to change it

read -p 'Git Http URL: ' giturl

if [[ -z "$giturl" ]]; then
        giturl=`git remote -v | head -n 1 | awk -F ' ' '{ print $2 }'`
        if [[ "$giturl" == *"@"* ]]; then
            giturl=`echo $giturl | awk -F '@' '{ print $2 }'`
            giturl="https://${giturl}"
        fi
fi

giturl=`sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//'<<<"${giturl}"`

echo
echo Now we need personal access token that has read or write or both access
echo Hint : click on profile on top right -\> Edit Profile -\> Access Token -\> Check write_reposity -\> Generate token
echo

read -p 'Access Token: ' token
token=`sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//'<<<"${token}"`

echo


echo do you want to apply these changes locally or globally?

read -p '[local/global] (default:local) :   ' state

if [ "$state" == "global" ]; then
    git config --global --replace-all remote.origin.url https://oauth2:${token}@${giturl:8}
    echo
    echo applying changes to remote.origin.url globally
    echo
else
    git config --local --replace-all remote.origin.url https://oauth2:${token}@${giturl:8}
    echo
    echo applying changes to remote.origin.url locally
    echo
fi

read -p "View updated git configs? [y/N] (default:y):     " view

if [[ $view == "n" || $view == "N" || $view == "no" || $view == "No" ]]
then
    echo okay bye!!! :\(
else
    echo
    git config --get-regexp .
fi
