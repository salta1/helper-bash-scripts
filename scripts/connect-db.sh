#!/usr/bin/bash

DIR_PATH=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

FILENAME="${DIR_PATH}/connect-db-secrets.txt"
if [ ! -f "$FILENAME" ] 
then
    echo "$FILENAME doesn't exists. Check the example and create a file with all the variables filled out and save it as $FILENAME"
	exit 1
fi

source "$FILENAME"
export PAGER="less -S -i"
export PGPASSWORD="$PGPASSWORD"


if [ "$#" == "0" ]
then
	psql -h "$AWS_CLOUD_DB_URL" --dbname="$DB_NAME"  --username="$USERNAME"
elif [ "$#" == "1" ]
then
	if [ "$1" == "-h" ]
	then
		echo "There are currently four use cases covered by this script:

1. No argument opens up the pgsql terminal to the 
   cloud instance: '$ connect-db.sh'
2. One argument that is a string -> runs the sql 
   script on the cloud: '$ connect-db \"SELECT * FROM mempt limit 10\"'
3. Passing a table with the m flag provides 
   the minimum fields to add when creating a 
   new entry in a table: '$ connect-db.sh -m mempt'
4. It can be used to see tables in two ways:
	a. Simply see all the 
	   tables: '$ connect-db.sh show tables'
	b. See tables with a substring in their 
	   name: '$ connect-db.sh show tables mempt'
5. You can peek at the first ten rows of a table 
   with:'$ connect-db.sh show table mempt'
6. See the most recent entries with: '$ connect-db show recent tsut'
7. You can see the schema with: '$ connect-db.sh show schema mempt'		

HINT: SQL EXAMPLES:
INSERT - \"INSERT INTO films (code, title) VALUES ('T_601', 'Yojimbo', 106)\"
UPDATE - \"UPDATE films SET kind = 'Dramatic',form='new' WHERE kind = 'Drama'\"
DELETE - \"DELETE FROM films USING producers WHERE producer_id = producers.id\"

To update - simply do '$ connect-db.sh update' and you'll have the latest version
"
	elif [ "$1" == "update" ]
	then
		echo "we are now updating the connect-db.sh"
		cd "$DIR_PATH"
		git fetch
		git pull 
	else
		psql -h "$AWS_CLOUD_DB_URL" --dbname="$DB_NAME"  --username="$USERNAME" -c "$1"
	fi
elif [ "$1" == "show" ] && [ "$2" == "tables" ] 
then
	if [ "$#" == "2" ]
	then
		connect-db.sh "\dt *.*"
	else
		out=$(connect-db.sh "\dt *.*") 
		echo "$out" | head -n3
		echo "$out" | grep "$3"
	fi
elif [ "$1" == "show" ] && [ "$2" == "table" ] 
then
	if [ "$#" == "2" ]
	then
		echo "you need to say what table to show - for instance 
to see mempt: $ connect-db.sh show table mempt"
	else
		connect-db.sh "SELECT * FROM $3 LIMIT 20"
	fi
elif [ "$1" == "show" ] && [ "$2" == "recent" ] 
then
	if [ "$#" == "2" ]
	then
		echo "you need to say what table's recent entries to show - for instance 
to see mempt: $ connect-db.sh show recent mempt"
	else
		connect-db.sh "SELECT * FROM $3 ORDER BY lchgdat DESC LIMIT 20 "
	fi
elif [ "$1" == "show" ] && [ "$2" == "schema" ] 
then
	if [ "$#" == "2" ]
	then
		echo "you need to say what table to show - for instance 
to see mempt: $ connect-db.sh show schema mempt"
	else
		connect-db.sh "\d+ $3"
	fi

else
	while getopts 'm:' flag; do
		case "${flag}" in 
			m) psql -h "$AWS_CLOUD_DB_URL" --dbname="$DB_NAME"  --username="$USERNAME" -c "SELECT column_name, data_type, udt_name, is_nullable, column_default FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name='${OPTARG}' and is_nullable='NO' and column_default IS NULL;";;
			*) echo "no flags recognised"
		esac
	done
fi
