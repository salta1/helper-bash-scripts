# Automation scripts

This project is meant to provide automation scripts to help others increase their productivity.


# Running scripts

## Without cloning
- for git-config-replacer :
```
bash <(curl -fsSL https://gitlab.com/salta1/helper-bash-scripts/-/raw/main/scripts/git-config-replacer.sh)
```

## With cloning

You can add the scripts folder to your path and and make it executable:

```
git clone https://gitlab.com/salta1/helper-bash-scripts.git
cd helper-bash-scripts/
cd scripts
sudo echo "export PATH=\"`pwd`:$PATH\"" >> ~/.bashrc
sudo chmod +x .
```